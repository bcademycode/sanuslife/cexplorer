# Colored Coins Block-Explorer

> The Colored Coins Block-Explorer

## Requirements

1. Bitcoin Core that runs as an RPC server with `-txindex=1`
2. MongoDB
3. The size of the data in the DB depends on `start_block`

## Configuring

On a first run, copy [`config/properties.sample.conf`](config/properties.sample.conf) as `config/explorer.conf`
and `config/scanner.conf` and edit the configuration parameters according to your needs.

Through environment variables it's possible to override configuration parameters.
Set `NODE_ENV` to `production` or `development` to change the environment in which cexplorer
should run.

## Installing and running locally

To install and run the service, execute:

```bash
$ npm install
$ NODE_ENV=production npm start
```

A Dockerfile based on node 8 Debian Buster is available.
To build and run a docker image of cexplorer, run:

## Build and running on docker
### Development environment
```bash
$ ./dev build
$ ./dev up
```

### Production environment with multiarch support and push to repository
```bash
$ MULTI_ARCH=yes ./dev build-prod
$ MULTI_ARCH=yes ./dev push-prod
```

## License

This software is licensed under the Apache License, Version 2.0.
See [LICENSE](LICENSE) for the full license text.
